# Ubuntu et Mint sur Asus ROG Strix GL702VMK

**Post-installation de Linux Ubuntu et Mint sur un Asus ROG GL702VMK : problèmes et solutions.**

Ce tuto a une visée personnelle : garder trace de ma config sur cet ordinateur et des problèmes rencontrés. Il a aussi une visée plus générale : rendre disponibles auprès d'un public francophone et dans un seul document des méthodes disponibles ailleurs, principalement en anglais. Enfin, comme beaucoup des bugs répertoriés ci-après sont liés à Nvidia, j'espère que ce travail pourra être utile à des utilisateurs et utilisatrices d'autres machines et distributions.

Méthode testée sur Ubuntu 17.10 Artful, Ubuntu 18.04 Bionic et Mint 19 Tara, principalement sous Cinnamon.

Ce tuto est une adaptation en français de plusieurs documents :

- Un tuto consacré spécifiquement aux problèmes rencontrés sous Ubuntu avec ce modèle d'ordinateur : <https://gist.github.com/GMMan/def55b688289f52b8635f1a83c25b1b5>
- Un commentaire sur la configuration de Grub2 : <https://askubuntu.com/questions/362722/how-to-fix-plymouth-splash-screen-in-all-ubuntu-releases/362998#362998>
- Diverses autres sources, qui seront indiquées au fur et à mesure.

**Pré-requis :**

Avoir installé Ubuntu ou Mint avec le support CSM et le secure boot désactivés (le CSM pour pouvoir switcher sans problème entre boot par support USB et boot sur disque dur, le secure boot pour pouvoir installer et paramétrer le pilote Nvidia). En revanche, le fast boot peut rester activé.

###  Améliorer le rendu graphique :

Le Asus ROG GL702VMK utilise la carte graphique Nvidia GeForce GTX 1600. Voici ses références complètes :

```
$ lspci | grep NVIDIA
01:00.0 VGA compatible controller: NVIDIA Corporation GP106M [GeForce GTX 1060 Mobile 6GB] (rev a1)
```


Une carte mal configurée peut entraîner divers problèmes :

-- un rendu sale des images et en particulier des dégradés ;

-- une impossibilité de sortir de veille ;

-- etc.

Pour l'essentiel, ces problèmes sont liés à l'utilisation du driver libre nouveau en lieu et place du driver propriétaire de Nvidia.

**1. Installer et activer le driver propriétaire nvidia-driver-390 :**

-- Vérifier s'il est déjà installé ou non : 

```
$ sudo dkms status
nvidia, 390.77, 4.15.0-45-generic, x86_64: installed
```


-- L'installer s'il n'apparaît pas dans la liste :

```
$ sudo apt install nvidia-driver-390
```

-- Redémarrer pour l'activer.

-- Si besoin, vérifier que le driver est bien activé (`Kernel driver in use`) :

```
$ lspci -vnn | grep -A 12 '\''[030[02]\]' | grep -Ei "vga|3d|display|kernel"  
01:00.0 VGA compatible controller [0300]: NVIDIA Corporation GP106M [GeForce GTX 1060 Mobile 6GB] [10de:1c60] (rev a1) (prog-if 00 [VGA controller])
	Kernel driver in use: nvidia
	Kernel modules: nvidiafb, nouveau, nvidia_drm, nvidia
```

Il est possible également d'installer et d'activer ce pilote en mode graphique :

-- Pour Ubuntu : <https://doc.ubuntu-fr.org/gestionnaire_de_pilotes_proprietaireshttps://doc.ubuntu-fr.org/nvidia>

-- Pour Mint : <https://linuxmint-installation-guide.readthedocs.io/fr/latest/drivers.html>

Pour en savoir plus pour la gestion des cartes Nvidia sous Ubuntu : <https://doc.ubuntu-fr.org/nvidia>

**2. Configurer Xorg :**

Une fois le pilote Nvidia installé et activé, il est nécessaire de configurer Xorg pour qu'il puisse travailler de concert avec ce dernier.

-- Générer le fichier `/etc/X11/xorg.conf` :

```
$ cd /etc/X11
$ sudo nvidia-xconfig
```

-- Dans la section `"screen"`, ajouter :

```
Option "FlatPanelProperties" "Dithering = Enabled"
```

Ce qui donne :



	Section "Screen"
	    Identifier     "Screen0"
	    Device         "Device0"
	    Monitor        "Monitor0"
	    DefaultDepth    24
	    Option         "FlatPanelProperties" "Dithering = Enabled"
	    SubSection     "Display"
	        Depth       24
	    EndSubSection
	EndSection

### Configurer Grub2

Si grub et le splash screen n'affichent pas la bonne résolution, si les combinaisons de touches Fn+F5 et Fn+F6 n'ont aucun effet sur la luminosité de l'écran, c'est ici que ça se passe :

**1. Régler la résolution du splash screen :**

-- Editer `/etc/default/grub` : 

```
$ sudo -H gedit /etc/default/grub
```

-- Localiser la variable : 

```
GRUB_GFXMODE=640x480
```

-- La remplacer par :

```
GRUB_GFXMODE=1920x1080
GRUB_GFXPAYLOAD_LINUX=keep
```

-- Sauvegarder, puis updater initramfs et le grub :

```
$ echo FRAMEBUFFER=y | sudo tee /etc/initramfs-tools/conf.d/splash
$ sudo update-initramfs -u
$ sudo update-grub
```

**2. Activer les combinaisons de touches Fn+F5 et Fn+F6 réglant la luminosité de l'écran :**

Encore une fois, ce problème est lié à Nvidia. Pour le résoudre, il faut rajouter l'option `"nvidia.NVreg_EnableBacklightHandler=1"` dans `/etc/default/grub` :

```
GRUB_CMDLINE_LINUX_DEFAULT="nvidia.NVreg_EnableBacklightHandler=1"
```

Sinon, une solution de contournement consiste à passer par `xbacklight`, mais c'est évidemment moins pratique.

**3. Autres réglages**

Il ne s'agit pas ici de résoudre des problèmes mais d'apporter quelques améliorations à l'apparence de Grub2 :

- Changer la couleur de grub (la remettre en violet) :

`/boot/grub/grub.cfg` fait appel à `/etc/grub.d/05_debian` qui lui-même appelle `/usr/share/plymouth/themes/default.grub`. Dans ce dernier fichier, il faut donc modifier les hexadécimaux déterminant `backgroud_color` :

	if background_color 40,0,33; then
		clear
	fi

-- Pour le noir : 0,0,0

-- Pour le gris clair : 60,59,55

-- etc.

Source : <https://forum.ubuntu-fr.org/viewtopic.php?id=1018291>

- Ajouter une image :

-- Placer son fichier au format .png, .jpg ou .tga (utiliser Gimp pour
l’export) dans le dossier `/usr/share/images/desktop-base`.

-- Dans `/etc/default/grub`, ajouter après `GRUB_CMDLINE_LINUX` cette
ligne (attention aux guillemets) :

```
GRUB_BACKGROUND="/usr/share/images/desktop-base/mon_image.png"
```

-- Mettre à jour Grub2 : 

```
$ sudo update-grub
```

- Supprimer le splash screen et faire défiler les messages système au boot de manière verbeuse :

-- Retirer les options `"quiet"` et `"splash"` à la variable `GRUB_CMDLINE_LINUX_DEFAULT`.

Quelques idées supplémentaires ici : <https://www.thegeekstuff.com/2012/10/grub-splash-image/>

### Autres problèmes

Ces problèmes ne sont pas directement liés au modèle de l'ordinateur, mais il peut être utile de les connaître ainsi que, s'il y en a, leurs solutions :

**1. Décalage de l’heure sous Windows en dual-boot :**

Ce problème bien connu se règle dans Windows (ici Windows 10 64 bits). 

-- Ouvrir une invite de commande en mode administrateur et ajouter une clé au registre :

```
Reg add HKLM\SYSTEM\CurrentControlSet\Control\TimeZoneInformation /v RealTimeIsUniversal /t REG_QWORD /d 1
```

-- Éventuellement, désactiver le réglage automatique de l’heure sur Internet.

-- Il est aussi possible de tenter de régler le problème sous Ubuntu.

Détails et explications ici : <https://openclassrooms.com/forum/sujet/dereglage-de-l-heure-sous-win10?page=1>

**2. Ralentissement de Mint avec certains composants KDE :**

Attention à Konqueror et Dolphin : leur indexeur baloo ralentit beaucoup
Linux Mint Tara Cinnamon

### Voir aussi

[My Ubuntu post-install script](https://gitlab.com/sakura-lain/my-ubuntu-postinstall-script)