# Ubuntu et Mint sur Asus ROG Strix GL702VMK

**Post-installation de Linux Ubuntu et Mint sur un Asus ROG GL702VMK : problèmes et solutions.**

Ce tuto a une visée personnelle : garder trace de ma config sur cet ordinateur et des problèmes rencontrés. Il a aussi une visée plus générale : rendre disponibles auprès d'un public francophone et dans un seul document des méthodes disponibles ailleurs, principalement en anglais. Enfin, comme beaucoup des bugs répertoriés ci-après sont liés à Nvidia, j'espère que ce travail pourra être utile à des utilisateurs et utilisatrices d'autres machines et distributions.

Méthode testée sur Ubuntu 17.10 Artful, Ubuntu 18.04 Bionic et Mint 19 Tara, principalement sous Cinnamon.

Ce tuto est une adaptation en français de plusieurs documents :

- Un tuto consacré spécifiquement aux problèmes rencontrés sous Ubuntu avec ce modèle d'ordinateur : <https://gist.github.com/GMMan/def55b688289f52b8635f1a83c25b1b5>
- Un commentaire sur la configuration de Grub2 : <https://askubuntu.com/questions/362722/how-to-fix-plymouth-splash-screen-in-all-ubuntu-releases/362998#362998>
- Diverses autres sources, qui seront indiquées au fur et à mesure.

**Pré-requis :**

Avoir installé Ubuntu ou Mint avec le support CSM et le secure boot désactivés (le CSM pour pouvoir switcher sans problème entre boot par support USB et boot sur disque dur, le secure boot pour pouvoir installer et paramétrer le pilote Nvidia). En revanche, le fast boot peut rester activé.

**[Voir le Wiki >>>](https://gitlab.com/sakura-lain/ubuntu-mint-asus-rog-gl702vmk/wikis/home)**

### Voir aussi

[My Ubuntu post-install script](https://gitlab.com/sakura-lain/my-ubuntu-postinstall-script)